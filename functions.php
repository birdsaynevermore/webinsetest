<?php
//Устанавка соединения с базой данных MySQL
$db_host = 'localhost';
$db_user = 'test_user';
$db_pass = '^65Njbx3';
$db_name = 'test_bd';
$link = mysqli_connect($db_host, $db_user, $db_pass, $db_name);
if (!$link) {
	$answer = [
		'status' => 'ok',
		'message' => '<div class="error notification">Can not connect to DB. Error code: ' . mysqli_connect_errno() . ', error: ' . mysqli_connect_error() . '</div>'
	];
	echo json_encode($answer);
	exit;
}
//Добавление или редактирование пользователя
if (isset($_POST["firstname"])) {
if (isset($_POST['id'])) {
	//Редактирование
	$sql = mysqli_query($link, "UPDATE `users` SET `firstname` = '{$_POST['firstname']}',`secondname` = '{$_POST['secondname']}',`email` = '{$_POST['email']}' WHERE `id`={$_POST['id']}");
} else {
	//Добавление
	$sql = mysqli_query($link, "INSERT INTO `users` (`firstname`, `secondname`, `email`) VALUES ('{$_POST['firstname']}', '{$_POST['secondname']}', '{$_POST['email']}')");
}
	if ($sql) {
		$answer = [
			'status' => 'ok',
			'message' => '<div class="done notification">Done</div>'
		];
		echo json_encode($answer);
	} else {
		$answer = [
			'status' => 'ok',
			'message' => '<div class="error notification">Error: ' . mysqli_error($link) . '</div>'
		];
		echo json_encode($answer);
	}
}
//Форма для редактирования пользователя
if (isset($_GET['user_id'])) {
	$sql = mysqli_query($link, "SELECT `id`, `firstname`, `secondname`, `email` FROM `users` WHERE `id`={$_GET['user_id']}");
	$user = mysqli_fetch_array($sql);
	$tableEdit = "<form action='' method='post' name='editUser' id='editUser' onsubmit='editFunc()'><table class='editingTable table'><thead><tr><th scope='col'>First name</th><th scope='col'>Second name</th><th scope='col'>E-mail</th><th></th></tr></thead><tbody><tr><td><input type='hidden' name='id' value='" . $user['id'] . "' /><input type='text' name='firstname' value='" . $user['firstname'] . "' required /></td><td><input type='text' name='secondname' value='" . $user['secondname'] . "' required /></td><td><input type='email' name='email' value='" . $user['email'] . "' pattern='[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$' required /></td><td><button type='submit'>Save<i class='far fa-save'></i></button></td></tr></tbody></table></form>";
	$answer = [
		'status' => 'ok',
		'message' => $tableEdit
	];
	echo json_encode($answer);
}
//Удаление пользователя
if (isset($_GET['del_id'])) {
	$sql = mysqli_query($link, "DELETE FROM `users` WHERE `id` = {$_GET['del_id']}");
	if ($sql) {
		$answer = [
			'status' => 'ok',
			'message' => '<div class="done notification">User deleted</div>'
		];
		echo json_encode($answer);
	} else {
		$answer = [
			'status' => 'ok',
			'message' => '<div class="error notification">Error: ' . mysqli_error($link) . '</div>'
		];
		echo json_encode($answer);
	}
}
//Загрузка таблицы из базы данных во фронтэнд
if (isset($_POST["load"])) {
	$sql = mysqli_query($link, 'SELECT `id`, `firstname`, `secondname`, `email` FROM `users`');
	$sqlTable = "<table class='contentTable table'><thead><tr><th scope='col'>First name</th><th scope='col'>Second name</th><th scope='col'>E-mail</th><th scope='col'>Edit</th><th scope='col'>Delete</th></tr></thead><tbody>";
	while ($result = mysqli_fetch_array($sql)) {
		$sqlTable .= "<tr><td>{$result['firstname']}</td><td>{$result['secondname']}</td><td>{$result['email']}</td><td><i class='fas fa-user-edit' onclick='editUser({$result['id']})'></i></td><td><i class='fas fa-user-times' onclick='deleteUser({$result['id']})'></i></td></tr>";
	}
	$sqlTable .= "</tbody></table>";
	$answer = [
		'status' => 'ok',
		'message' => $sqlTable
	];
	echo json_encode($answer);
}
?>
    
