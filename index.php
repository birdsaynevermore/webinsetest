<!doctype html>
<html lang="ru-RU">
	<head>
		<meta charset="UTF-8">
		<meta firstname="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link href="css/fontawesome.css" rel="stylesheet">
		<link rel="stylesheet" href="css/style.css">
		<script src="js/jquery.min.js"></script>
		<title>User Edit</title>
	</head>
	<body>
		<div id="dbTable">
		</div>
		<form action="" method="post" name="addUser" id="addUser">
			<table class="editingTable table">
				<thead>
					<tr>
						<th colspan="4" scope="col">Add new user</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<input type="text" name="firstname" value="" placeholder="First name *" required />
						</td>
						<td>
							<input type="text" name="secondname" value="" placeholder="Second name *" required />
						</td>
						<td>
							<input type="email" name="email" value="" placeholder="E-mail *" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" required />
						</td>
						<td>
							<button type="submit">Save<i class="far fa-save"></i></button>
						</td>
					</tr>
				</tbody>
			</table>
		</form>
		<div id="dbEdit">
		</div>
		<script type="text/javascript">
			//Вызываем функцию отображения таблицы после загрузки стриницы
			$(document).ready(function () {
				loadTable();
			});
			//Загружаем таблицу
			function loadTable() {
				$.ajax({
					type: "POST",
					url: "functions.php",
					data: {
						load: "load"
					},
					success: function(response) {
						var result = JSON.parse(response);
						$("#dbTable").html(result.message);
					},
				})
			}
			//Добавление пользователя
			$(document).ready(function() {
				var formAdd = $("#addUser");
				formAdd.submit(function(event) {
					event.preventDefault();
					$.ajax({
						url: "functions.php",
						type: "POST",                        
						data: formAdd.serialize(),
						success: function(response) {
							var result = JSON.parse(response);
							$("#dbEdit").html(result.message);
							loadTable();
						},
					});
				})
			});
			//Удаление пользователя
			function deleteUser(idUser) {
				var isDelete = confirm("Delete user?");
				if(isDelete==true) {
					$.ajax({
						url: "functions.php",
						type: "GET",                        
						data: {
							del_id: idUser
						},
						success: function(response) {
							var result = JSON.parse(response);
							$("#dbEdit").html(result.message);
							loadTable();
						},
					});
				}
			}
			//Вызов формы редактирования пользователя
			function editFunc() {
				var formEdit = $("#editUser");
				event.preventDefault();
				$.ajax({
					url: "functions.php",
					type: "POST",                        
					data: formEdit.serialize(),
					success: function(response) {
						var result = JSON.parse(response);
						$("#dbEdit").html(result.message);
						loadTable();
					},
				});
			}
			//Редактирование пользователя
			function editUser(idUser) {
				$.ajax({
					url: "functions.php",
					type: "GET",                        
					data: {
						user_id: idUser
					},
					success: function(response) {
						var result = JSON.parse(response);
						$("#dbEdit").html(result.message);
						loadTable();
					},
				});
			}
		</script>
	</body>
</html>